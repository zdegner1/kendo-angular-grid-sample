import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DocumentsComponent } from './components/documents/documents.component';
import {LocalStorageService} from './services/local.storage.service';
import {GridSettingsService} from './services/grid.settings.service';

import { MatToolbarModule, MatCardModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';

import { GridModule } from '@progress/kendo-angular-grid';

@NgModule({
  declarations: [
    AppComponent,
    DocumentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    GridModule,
  ],
  providers: [LocalStorageService, GridSettingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
