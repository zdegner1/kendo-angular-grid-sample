import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() { }

  private getLocalStorage() {
    return (typeof window !== 'undefined') ? window.localStorage : null;
  }

  public put(key: string, value: string): void {
    const localStorage = this.getLocalStorage();
    if (localStorage) {
      localStorage.setItem(key, value);
    }
  }

  public get(key: string): string {
    const localStorage = this.getLocalStorage();
    if (localStorage) {
      return localStorage.getItem(key);
    }

    return null;
  }
}
