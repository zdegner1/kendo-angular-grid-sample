import {Injectable} from '@angular/core';
import {LocalStorageService} from './local.storage.service';
import {State} from '@progress/kendo-data-query';

@Injectable()
export class GridSettingsService {

  static readonly SETTINGS_KEY = 'gridSettings.';

  constructor(private localStorage: LocalStorageService) { }

  public load(gridName: string): State {
    const jsonSettings: string = this.localStorage.get(GridSettingsService.SETTINGS_KEY + gridName);
    if (jsonSettings) {
      return JSON.parse(jsonSettings);
    }

    return {};
  }

  public save(gridName: string, settings: State): void {
    this.localStorage.put(GridSettingsService.SETTINGS_KEY + gridName, JSON.stringify(settings));
  }
}
