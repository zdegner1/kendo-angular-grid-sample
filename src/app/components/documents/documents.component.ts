import { Component, OnInit } from '@angular/core';
import { process, State } from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult} from '@progress/kendo-angular-grid';
import {sampleDocuments} from '../../data/sample.documents';
import {GridSettingsService} from '../../services/grid.settings.service';
import {SelectionEvent} from '@progress/kendo-angular-grid/dist/es2015/selection/selection.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent {

  public state: State = {};
  public gridData: GridDataResult;
  public selectedDocument: any;

  constructor(private gridSettingsService: GridSettingsService) {
    const savedState = gridSettingsService.load('myGridName'); // load the grid settings
    this.dataStateChange(savedState);
  }

  public dataStateChange(state: State): void {
    this.state = state;

    this.gridData = process(sampleDocuments, this.state); // process(...) will apply client side filtering, sorting, etc.

    this.gridSettingsService.save('myGridName', this.state); // save the current grid settings
  }

  public selectionChange(selection: SelectionEvent): void {
    this.selectedDocument = (selection.selectedRows.length > 0) ? selection.selectedRows[0].dataItem : null;
  }
}
